package controller;

import model.persistance.IPersistance;
import model.persistance.IUserPersistance;
import model.User;

/**
 * controller for users
 * @author Clara Zufall
 * TODO implement this class
 * �berarbeitet von @author Luca Jung
 */
public class UserController {

	private IUserPersistance userPersistance;
	
	public UserController(IPersistance userPersistance) {
		this.userPersistance = userPersistance.getUserPersistance();
	}
	
	/**
	 * screibt den gegebene benutzer in die datenbank solange dieser nicht bereits existiert
	 * @param user Der zu erzeugende Benutzer.
	 */
	public void createUser(User user) {
		if(this.checkForUser(user)) {
			this.userPersistance.createUser(user);
		}
	}
	
	/**
	 * liest einen User aus der Persistenzschicht und gibt das Userobjekt zur�ck
	 * @param username eindeutige Loginname
	 * @param passwort das richtige Passwort
	 * @return Userobjekt, null wenn der User nicht existiert
	 */
	public User readUser(String username, String passwort) {
		return this.userPersistance.readUser(username);
	}
	
	/**
	 * erneuert/�berschreibt alle daten des gegeben benutzers
	 * @param user Der zu erneuernde Benutzer
	 */
	public void changeUser(User user) {
		if(this.checkForUser(user)) {
			this.userPersistance.updateUser(user);
		}
	}
	
	/**
	 * l�scht den gegeben benutzer, falls er exisitert.
	 * @param user Der zu l�schende Benutzer.
	 */
	public void deleteUser(User user) {
		if(this.checkForUser(user)) {
			this.userPersistance.deleteUser(user);
		}
	}
	
	/**
	 * �berp�ft ob das gegebene passwort dem des gegebenen usernamen entspricht 
	 * @param username Der Username des Benutzers der gepr�ft wird.
	 * @param passwort Das Passwort das gepr�ft werden soll.
	 * @return true oder false entsprechend dem zutreffen des Passwortes
	 */
	public boolean checkPassword(String username, String passwort) {
		User user = this.readUser(username, passwort);
		return user.getPassword().equals(passwort);
	}
	
	/*
	 * Hilfsmethode
	 */
	private boolean checkForUser(User user) {
		return this.userPersistance.readUser(user.getLoginname()) != null;
	}
}
