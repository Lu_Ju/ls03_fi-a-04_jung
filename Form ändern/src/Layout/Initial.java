package Layout;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.Box;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JColorChooser;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import java.awt.Color;

public class Initial extends JFrame {

	private JPanel panel;
	private JTextField tfdA2Text;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Initial window = new Initial();
					window.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Initial() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		setTitle("Form �ndern");
		setBounds(100, 100, 450, 694);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		panel = new JPanel();
		setContentPane(panel);
		getContentPane().setLayout(null);
		
		JLabel lblMain = new JLabel("Dieser Text soll ver\u00E4ndert werden.");
		lblMain.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblMain.setHorizontalAlignment(SwingConstants.CENTER);
		lblMain.setBounds(10, 11, 414, 70);
		getContentPane().add(lblMain);
		
		JLabel lblA1Aufgabe1 = new JLabel("Aufgabe 1: Hintergrundfarbe \u00E4ndern");
		lblA1Aufgabe1.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblA1Aufgabe1.setBounds(10, 85, 245, 28);
		getContentPane().add(lblA1Aufgabe1);
		
		JButton btnA1Rot = new JButton("Rot");
		btnA1Rot.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnA1Rot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panel.setBackground(Color.RED);
			}
		});
		btnA1Rot.setBounds(10, 124, 130, 35);
		getContentPane().add(btnA1Rot);
		
		JButton btnA1Gr�n = new JButton("Gr�n");
		btnA1Gr�n.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnA1Gr�n.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel.setBackground(Color.GREEN);
			}
		});
		btnA1Gr�n.setBounds(150, 124, 130, 35);
		getContentPane().add(btnA1Gr�n);
		
		JButton btnA1Blau = new JButton("Blau");
		btnA1Blau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panel.setBackground(Color.BLUE);
			}
		});
		btnA1Blau.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnA1Blau.setBounds(290, 124, 130, 35);
		getContentPane().add(btnA1Blau);
		
		JButton btnA1Gelb = new JButton("Gelb");
		btnA1Gelb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel.setBackground(Color.YELLOW);
			}
		});
		btnA1Gelb.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnA1Gelb.setBounds(10, 170, 130, 35);
		getContentPane().add(btnA1Gelb);
		
		JButton btnA1Standart = new JButton("Standartfarbe");
		btnA1Standart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel.setBackground(UIManager.getColor("menu"));
			}
		});
		btnA1Standart.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnA1Standart.setBounds(150, 170, 130, 35);
		getContentPane().add(btnA1Standart);
		
		JButton btnA1FW�hlen = new JButton("Farbe w�hlen");
		btnA1FW�hlen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel.setBackground(JColorChooser.showDialog(lblMain, "Neue Farbe w�hlen", Color.white));
			}
		});
		btnA1FW�hlen.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnA1FW�hlen.setBounds(290, 170, 130, 35);
		getContentPane().add(btnA1FW�hlen);
		
		JLabel lblA2Aufgabe2 = new JLabel("Aufgabe 2: Text formartieren");
		lblA2Aufgabe2.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblA2Aufgabe2.setBounds(10, 216, 245, 28);
		getContentPane().add(lblA2Aufgabe2);
		
		JButton btnA2Arial = new JButton("Arial");
		btnA2Arial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {		
				lblMain.setFont(new Font("Arial", lblMain.getFont().getStyle(), lblMain.getFont().getSize()));
			}
		});
		btnA2Arial.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnA2Arial.setBounds(10, 247, 130, 35);
		getContentPane().add(btnA2Arial);
		
		JButton btnA2ComicSansMs = new JButton("Comic Sans MS");
		btnA2ComicSansMs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblMain.setFont(new Font("Comic Sana MS", lblMain.getFont().getStyle(), lblMain.getFont().getSize()));
			}
		});
		btnA2ComicSansMs.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnA2ComicSansMs.setBounds(150, 247, 130, 35);
		getContentPane().add(btnA2ComicSansMs);
		
		JButton btnA2CourierNew = new JButton("Courier New");
		btnA2CourierNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblMain.setFont(new Font("Courier New", lblMain.getFont().getStyle(), lblMain.getFont().getSize()));
			}
		});
		btnA2CourierNew.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnA2CourierNew.setBounds(290, 247, 130, 35);
		getContentPane().add(btnA2CourierNew);
		
		tfdA2Text = new JTextField();
		tfdA2Text.setText("Hier bitte Text eingeben");
		tfdA2Text.setBounds(10, 293, 414, 20);
		getContentPane().add(tfdA2Text);
		tfdA2Text.setColumns(10);
		
		JButton btnA2LSchreiben = new JButton("Ins Label schreiben");
		btnA2LSchreiben.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblMain.setText(tfdA2Text.getText());	
			}
		});
		btnA2LSchreiben.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnA2LSchreiben.setBounds(10, 324, 198, 23);
		getContentPane().add(btnA2LSchreiben);
		
		JButton btnA2LL�schen = new JButton("Text im Label l\u00F6schen");
		btnA2LL�schen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblMain.setText("");	
			}
		});
		btnA2LL�schen.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnA2LL�schen.setBounds(218, 324, 198, 23);
		getContentPane().add(btnA2LL�schen);
		
		JLabel lblA3Aufgabe3 = new JLabel("Aufgabe 3: Schriftfarbe \u00E4ndern");
		lblA3Aufgabe3.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblA3Aufgabe3.setBounds(10, 358, 245, 28);
		getContentPane().add(lblA3Aufgabe3);
		
		JButton btnA3Rot = new JButton("Rot");
		btnA3Rot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblMain.setForeground(Color.RED);
			}
		});
		btnA3Rot.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnA3Rot.setBounds(10, 386, 130, 35);
		getContentPane().add(btnA3Rot);
		
		JButton btnA3Blau = new JButton("Blau");
		btnA3Blau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblMain.setForeground(Color.BLUE);
			}
		});
		btnA3Blau.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnA3Blau.setBounds(150, 386, 130, 35);
		getContentPane().add(btnA3Blau);
		
		JButton btnA3Schwarz = new JButton("Schwarz");
		btnA3Schwarz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblMain.setForeground(Color.BLACK);
			}
		});
		btnA3Schwarz.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnA3Schwarz.setBounds(294, 386, 130, 35);
		getContentPane().add(btnA3Schwarz);
		
		JLabel lblA4Aufgabe4 = new JLabel("Aufgabe 4: Schriftgr\u00F6\u00DFe ver\u00E4ndern");
		lblA4Aufgabe4.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblA4Aufgabe4.setBounds(10, 432, 245, 28);
		getContentPane().add(lblA4Aufgabe4);
		
		JButton btnA4Plus = new JButton("+");
		btnA4Plus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblMain.setFont(new Font(lblMain.getFont().getFamily(), lblMain.getFont().getStyle(), lblMain.getFont().getSize()+1));
			}
		});
		btnA4Plus.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnA4Plus.setBounds(10, 459, 198, 23);
		getContentPane().add(btnA4Plus);
		
		JButton btnA4Minus = new JButton("-");
		btnA4Minus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblMain.setFont(new Font(lblMain.getFont().getFamily(), lblMain.getFont().getStyle(), lblMain.getFont().getSize()-1));
			}
		});
		btnA4Minus.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnA4Minus.setBounds(226, 459, 198, 23);
		getContentPane().add(btnA4Minus);
		
		JLabel lblA5Aufgabe5 = new JLabel("Aufgabe 5: Textausrichtung");
		lblA5Aufgabe5.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblA5Aufgabe5.setBounds(10, 493, 245, 28);
		getContentPane().add(lblA5Aufgabe5);
		
		JButton btnA5Linksbuendig = new JButton("linksb\u00FCndig");
		btnA5Linksbuendig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblMain.setHorizontalAlignment(SwingConstants.LEFT);
			}
		});
		btnA5Linksbuendig.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnA5Linksbuendig.setBounds(10, 532, 130, 35);
		getContentPane().add(btnA5Linksbuendig);
		
		JButton btnA5Zentriert = new JButton("zentriert");
		btnA5Zentriert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblMain.setHorizontalAlignment(SwingConstants.CENTER);
			}
		});
		btnA5Zentriert.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnA5Zentriert.setBounds(150, 532, 130, 35);
		getContentPane().add(btnA5Zentriert);
		
		JButton btnA5Rechtsbuendig = new JButton("rechtsb\u00FCndig");
		btnA5Rechtsbuendig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblMain.setHorizontalAlignment(SwingConstants.RIGHT);
			}
		});
		btnA5Rechtsbuendig.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnA5Rechtsbuendig.setBounds(290, 532, 130, 35);
		getContentPane().add(btnA5Rechtsbuendig);
		
		JLabel lblA6Aufgabe6 = new JLabel("Aufgabe 6: Programm beenden");
		lblA6Aufgabe6.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblA6Aufgabe6.setBounds(10, 576, 245, 28);
		getContentPane().add(lblA6Aufgabe6);
		
		JButton btnA6Exit = new JButton("EXIT");
		btnA6Exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(1);
			}
		});
		btnA6Exit.setForeground(Color.BLACK);
		btnA6Exit.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnA6Exit.setBounds(10, 615, 380, 29);
		getContentPane().add(btnA6Exit);
	}
}
